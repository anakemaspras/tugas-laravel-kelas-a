<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/page', function () {
    $nama='Rizqi';
    return view('page',['nama' => $nama]);
});

Route::get('/page2', function () {
    $daftarBarang=['Gula','Kopi', 'Roti', 'Teh', 'Mie Instan'];
    return view('page2', ['daftarBarang' => $daftarBarang]);
});
